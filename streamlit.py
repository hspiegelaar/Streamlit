import streamlit as st
import pandas as pd
import numpy as np

st.title('Hello World')

DATE_COLUMN = 'date/time'
DATA_URL = ('https://s3-us-west-2.amazonaws.com/'
         'streamlit-demo-data/uber-raw-data-sep14.csv.gz')

@st.cache
def load_data(nrows):
    data = pd.read_csv(DATA_URL, nrows=nrows)
    lowercase = lambda x: str(x).lower()
    data.rename(lowercase, axis='columns', inplace=True)
    data[DATE_COLUMN] = pd.to_datetime(data[DATE_COLUMN])
    return data


# Create a text element and let the reader know the data is loading.
data_load_state = st.text('Loading data...')
# Load 10,000 rows of data into the dataframe.
data = load_data(10000)
# Notify the reader that the data was successfully loaded.
data_load_state.text('Loading data...done!')


st.subheader('Raw data')
st.sidebar.subheader('Sidebar')


if st.sidebar.checkbox('Show raw data'):
    st.subheader('Raw data')
    st.write(data)


st.subheader('Number of pickups by hour')


hist_values = np.histogram(
    data[DATE_COLUMN].dt.hour, bins=24, range=(0,24))[0]


st.bar_chart(hist_values)


_min = min(data['lat'])
_max = max(data['lat'])

latmin = st.sidebar.slider('lat_min', _min, _max, (_min + _max)/2)
latmax = st.sidebar.slider('lat_max', _min, _max, (_min + _max)/2)

if latmin < latmax:

  n = len(data[(data['lat'] < latmax)&(data['lat'] > latmin)])
  st.text('aantal entries tussen lat {:.2f} en {:.2f}: {}'.format(latmin, latmax, n))

else:

  st.text('problem: min > max')


hour_to_filter = st.slider('hour', 0, 23, 17)  # min: 0h, max: 23h, default: 17h
st.text(hour_to_filter)



